'use strict';

describe('module: stream, controller: StreamCtrl', function () {

  // load the controller's module
  beforeEach(module('stream'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var StreamCtrl;
  beforeEach(inject(function ($controller) {
    StreamCtrl = $controller('StreamCtrl');
  }));

  it('should do something', function () {
    expect(!!StreamCtrl).toBe(true);
  });

});
