'use strict';

describe('module: jurados, controller: JuradosCtrl', function () {

  // load the controller's module
  beforeEach(module('jurados'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var JuradosCtrl;
  beforeEach(inject(function ($controller) {
    JuradosCtrl = $controller('JuradosCtrl');
  }));

  it('should do something', function () {
    expect(!!JuradosCtrl).toBe(true);
  });

});
