'use strict';

describe('module: main, service: Push', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Push;
  beforeEach(inject(function (_Push_) {
    Push = _Push_;
  }));

  it('should do something', function () {
    expect(!!Push).toBe(true);
  });

});
