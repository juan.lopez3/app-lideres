'use strict';

describe('module: formulario, controller: FormularioCtrl', function () {

  // load the controller's module
  beforeEach(module('formulario'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var FormularioCtrl;
  beforeEach(inject(function ($controller) {
    FormularioCtrl = $controller('FormularioCtrl');
  }));

  it('should do something', function () {
    expect(!!FormularioCtrl).toBe(true);
  });

});
