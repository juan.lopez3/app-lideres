'use strict';

describe('module: ganadores, controller: GanadoresCtrl', function () {

  // load the controller's module
  beforeEach(module('ganadores'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var GanadoresCtrl;
  beforeEach(inject(function ($controller) {
    GanadoresCtrl = $controller('GanadoresCtrl');
  }));

  it('should do something', function () {
    expect(!!GanadoresCtrl).toBe(true);
  });

});
