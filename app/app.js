'use strict';
angular.module('lideresSemana', [
  'ngStorage',
  // load your modules here
  'main', // starting with the main module
  'registro',
  'stream',
  'ganadores',
  'jurados',
  'formulario',
  'home',
  'terminos',
  'reglas',
  'reconocimiento'
]);
