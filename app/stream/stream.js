'use strict';
angular.module('stream', [
  'ionic',
  'ngCordova',
  'ui.router'
])
.config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.stream', {
    url: '/stream',
    views: {
      'pageContent': {
        templateUrl: 'stream/templates/stream.html',
        controller: 'StreamCtrl as ctrl'
      }
    }
  });
});
