'use strict';
angular.module('terminos', [
  'ionic',
  'ngCordova',
  'ui.router'
])
  .config(function ($stateProvider) {
    var state = $stateProvider;

    state.state('main.terminos', {
      url: '/terminos',
      views: {
        'pageContent': {
          templateUrl: 'terminos/templates/terminos.tpl.html'
        }
      }
    })
  })
