'use strict';
angular.module('formulario', [
  'ionic',
  'ngCordova',
  'ui.router'
])
  .config(function ($stateProvider) {

    $stateProvider.state('main.registroNominador', {
      url: '/formulario/nominador',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/nominador.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.registroNominado', {
      url: '/formulario/nominado',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/nominado.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });
    
    $stateProvider.state('main.pregunta0', {
      url: '/formulario/nominado/pregunta0',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta0.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta1', {
      url: '/formulario/nominado/pregunta1',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta1.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta2', {
      url: '/formulario/nominado/pregunta2',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta2.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta3', {
      url: '/formulario/nominado/pregunta3',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta3.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta4', {
      url: '/formulario/nominado/pregunta4',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta4.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta5', {
      url: '/formulario/nominado/pregunta5',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta5.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });

    $stateProvider.state('main.pregunta6', {
      url: '/formulario/nominado/pregunta6',
      views: {
        'pageContent': {
          templateUrl: 'formulario/templates/pregunta6.html',
          controller: 'FormularioCtrl as ctrl'
        }
      }
    });
  });
