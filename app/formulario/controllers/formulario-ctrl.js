'use strict';
angular.module('formulario').controller('FormularioCtrl', function ($state, $firebase, $message, $localStorage, $filter, $ionicHistory) {

  var ctrl = this;

  ctrl.usuario = $localStorage.usuario;
  
  ctrl.temporal = $localStorage.nominado || {};
  ctrl.temporal.info = ctrl.temporal.info || {};
  ctrl.temporal.preguntas = ctrl.temporal.preguntas || {};
  if(ctrl.temporal.info.nacimiento) ctrl.temporal.info.nacimiento = new Date(ctrl.temporal.info.nacimiento);

  ctrl.tipo = function (tipo, relacion) {
    if(tipo == undefined) return;
    
    if(tipo){
      $localStorage.nominado = {};
      if(relacion) $localStorage.nominado.relacion = relacion;
    } else {
      $localStorage.nominado = {info: $localStorage.usuario};
    }
    
    $state.go('main.registroNominado');
  }
  
  ctrl.limpiar = function () {
    delete $localStorage.nominado;
    ctrl.temporal = {};
  }

  ctrl.siguiente = function (estado) {
    $localStorage.nominado = ctrl.temporal;
    $state.go(estado);
  }

  ctrl.terminar = function (terminos) {
    if(!terminos)
      return $message.popup("Alerta", "Por favor acepte los términos y condiciones para continuar");
    
    var nominado = $localStorage.nominado;
    nominado.fecha = $filter('date')(new Date(), "dd-MM-yyyy HH:mm");
    nominado.info.nacimiento = $filter('date')(nominado.info.nacimiento, "dd-MM-yyyy");
    nominado.tipo = "app";
    delete nominado.info.push;
    delete nominado.info.uid;
    console.log(nominado);
    $firebase.push(['nominaciones'], nominado);
    delete $localStorage.nominado;
    
    $message.popup("Gracias por la nominación", "Los datos se han almacenado exitosamente.").then(function () {
      $ionicHistory.nextViewOptions({disableBack: true});
      $state.go('main.home');
    });
    
  }

});
