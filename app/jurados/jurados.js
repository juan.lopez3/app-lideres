'use strict';
angular.module('jurados', [
  'ionic',
  'ngCordova',
  'ui.router'
])

  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.jurados', {
    url: '/jurados',
    views: {
      'pageContent':{
        templateUrl: 'jurados/templates/jurados.tpl.html',
        controller: 'JuradosCtrl as ctrl'
      }
    },
  });

});
