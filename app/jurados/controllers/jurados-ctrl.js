'use strict';
angular.module('jurados').controller('JuradosCtrl', function ($firebase, $message) {

  var ctrl = this;

  $message.loading();
  
  $firebase.get(['jurados']).then(function (jurados) {
    ctrl.jurados = jurados;
    $message.hide();
  });
  
  

});
