'use strict';
angular.module('reglas', [
  'ionic',
  'ngCordova',
  'ui.router'
])

  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.reglas', {
    url: '/reglas',
    views: {
      'pageContent':{
        templateUrl: 'reglas/templates/reglas.tpl.html',
        controller: 'ReglasCtrl as ctrl'
      }
    },
  });

});
