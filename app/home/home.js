'use strict';
angular.module('home', [
  'ionic',
  'ngCordova',
  'ui.router'
])
  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.home', {
    url: '/home',
    views: {
      'pageContent': {
        templateUrl: 'home/templates/home.tpl.html',
        controller: 'HomeCtrl as ctrl'
      }
    }
  });
});
