'use strict';
angular.module('reconocimiento', [
  'ionic',
  'ngCordova',
  'ui.router'
])

  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.reconocimiento', {
    url: '/reconocimiento',
    views: {
      'pageContent':{
        templateUrl: 'reconocimiento/templates/reconocimiento.tpl.html',
        controller: 'ReconocimientoCtrl as ctrl'
      }
    },
  });

});
