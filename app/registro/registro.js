'use strict';
angular.module('registro', [
  'ionic',
  'ngCordova',
  'ui.router'
])
  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.login', {
    url: '/login',
    views: {
      'pageContent': {
        templateUrl: 'registro/templates/login.html',
        controller: 'LoginCtrl as ctrl'
      }
    }
  });

  state.state('main.registro', {
    url: '/registro',
    views: {
      'pageContent': {
        templateUrl: 'registro/templates/registro.html',
        controller: 'RegistroCtrl as ctrl'
      }
    }
  });
});
