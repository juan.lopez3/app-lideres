'use strict';
angular.module('registro').controller('LoginCtrl', function ($state, $firebase, $message, $localStorage, $push) {

  var ctrl = this;

  ctrl.login = function () {
    if(!ctrl.usuario || !ctrl.password)
      return $message.alert("Debe completar los dos campos");

    $firebase.login(ctrl.usuario, ctrl.password).then(function (uid) {
      $firebase.get(['usuarios', uid]).then(function (usuario) {
        $localStorage.usuario = usuario;
        $localStorage.usuario.uid = uid;
        $push.registro(uid);
        $state.go('main.registroNominador');
      });
    }).catch(function () {
      $message.alert("Email o documento incorrecto, por favor verifíquelos e intentelo nuevamente");
      ctrl.password = "";
    });
  }

});
