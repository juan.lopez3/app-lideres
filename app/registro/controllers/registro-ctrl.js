'use strict';
angular.module('registro').controller('RegistroCtrl', function ($firebase, $message, $push, $localStorage, $state) {

  var ctrl = this;

  ctrl.usuario = {};

  function validar (campo) {
    return (!campo || campo != "") ? false : true;
  }

  ctrl.registrar = function () {
    if( validar(ctrl.usuario.nombres) || validar(ctrl.usuario.primerAp) || validar(ctrl.usuario.segundoAp) || validar(ctrl.usuario.genero) || validar(ctrl.usuario.tipoDocumento) || validar(ctrl.usuario.documento) || validar(ctrl.usuario.email) || validar(ctrl.usuario.celular) || validar(ctrl.usuario.telefono) || validar(ctrl.usuario.departamento) || validar(ctrl.usuario.ciudad))
      return $message.alert("Por favor complete todos los campos");

    $firebase.createUser(ctrl.usuario.email, ctrl.usuario.documento).then(function (uid) {
      $firebase.set(['usuarios', uid], ctrl.usuario);
      $localStorage.usuario = ctrl.usuario;
      $localStorage.usuario.uid = uid;
      $push.registro(uid);
      $state.go('main.registroNominador');
    }).catch(function () {

    });
  }

});
