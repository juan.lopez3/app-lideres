'use strict';
angular.module('ganadores').controller('GanadoresCtrl', function ($firebase, $message, $scope) {

  var ctrl = this;
  ctrl.anios = [];
  ctrl.ano = "2015";

  $message.loading();
  
  $firebase.get(['mejoresLideres']).then(function (lideres) {
    ctrl.lideres = lideres;
    for(var i in ctrl.lideres) {
      if( ctrl.anios.indexOf(ctrl.lideres[i].anio.toString()) == -1 )
        ctrl.anios.push(ctrl.lideres[i].anio.toString());
    }
    $message.hide();
  });
  
  ctrl.mostrarAno = function () {
    window.plugins.actionsheet.show({
      title: "Selccione un año",
      buttonLabels: ctrl.anios,
      androidTheme: window.plugins.actionsheet.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }, function (index) {
      ctrl.ano = ctrl.anios[index-1];
      $scope.$apply();
    });
  }
  
});

angular.module('ganadores').controller('GanadorCtrl', function ($stateParams) {
  
  var ctrl = this;
  
  ctrl.lider = $stateParams.lider;

  console.log(ctrl.lider);

});
