'use strict';
angular.module('ganadores', [
  'ionic',
  'ngCordova',
  'ui.router'
])

  .config(function ($stateProvider) {
  var state = $stateProvider;

  state.state('main.ganadores', {
    url: '/ganadores',
    views: {
      'pageContent':{
        templateUrl: 'ganadores/templates/ganadores.tpl.html',
        controller: 'GanadoresCtrl as ctrl'
      }
    },
  });

  state.state('main.ganador', {
    url: '/ganadores',
    views: {
      'pageContent':{
        templateUrl: 'ganadores/templates/ganador.tpl.html',
        controller: 'GanadorCtrl as ctrl'
      }
    },
    params: { lider: null }
  });

});
