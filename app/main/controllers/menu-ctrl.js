'use strict';
angular.module('main').controller('MenuCtrl', function ($scope, $firebase, $localStorage) {
  
  $scope.isLogged = ($localStorage.usuario) ? true : false;

  $firebase.get(['menu']).then(function (menu) {
    $scope.menu = menu;
    $scope.$apply();
  });

});
