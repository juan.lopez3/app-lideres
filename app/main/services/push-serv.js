'use strict';
angular.module('main').service('$push', function ($firebase) {
  //AIzaSyAWv8aCyf2NBHln0OydV4wbAECloJaa3Vo
  //10891509783
  
  var service = {};
  var push = null;
  
  document.addEventListener('deviceReady', function () {
    
    push = PushNotification.init({
      android: { senderID: "10891509783", forceShow: true },
      ios: { alert: true, badge: true, sound: true, clearBadge: true }
    });
    
    push.on('registration', function (data) {
      push.token = data.registrationId;
      service.registro();
    });
    
    push.on('notification', function(data) {
      if(data.additionalData.foreground)
        return;
      
      console.info(data.additionalData.estado);
      $state.go(data.additionalData.estado);
    });
    
  }, false);

  service.registro = function (uid) {
    PushNotification.hasPermission(function() {
      $firebase.set(['usuarios', uid, 'push'], {type: ionic.Platform.platform(), token: push.token});
    });
  }

  return service;

});
