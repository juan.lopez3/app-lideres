'use strict';
angular.module('main').factory("$firebase", function ($q) {

  firebase.initializeApp({ apiKey: "AIzaSyC0gYsJyCUfI0en_f-W2s1JLm3sDxNpvQI", authDomain: "lideres-semana-4ba62.firebaseapp.com", databaseURL: "https://lideres-semana-4ba62.firebaseio.com", storageBucket: "lideres-semana-4ba62.appspot.com"});

  var service = this;

  var auth = firebase.auth();
  var database = firebase.database();
  var storage = firebase.storage();

  /*  _         _   _
     / \  _   _| |_| |__
    / _ \| | | | __| '_ \
   / ___ \ |_| | |_| | | |
  /_/   \_\__,_|\__|_| |_|*/

  service.createUser = function (email, password) {
    return $q(function (resolve, reject) {
      auth.createUserWithEmailAndPassword(email, password).then(function (user) {
        resolve(user.uid);
      }).catch(function (error) {
        console.log(error);
        reject(error);
      });
    });
  }

  service.login = function (email, password) {
    return $q(function (resolve, reject) {
      auth.signInWithEmailAndPassword(email, password).then(function (user) {
        resolve(user.uid);
      }).catch(function (error) {
        reject(error);
      });
    });
  }

  service.getAuth = auth.currentUser;

  service.logout = function () {
    return $q(function (resolve, reject) {
      auth.signOut.then(function () {
        resolve();
      }, function (error) {
        reject();
      });
    });
  }

  /*____        _        _
   |  _ \  __ _| |_ __ _| |__   __ _ ___  ___
   | | | |/ _` | __/ _` | '_ \ / _` / __|/ _ \
   | |_| | (_| | || (_| | |_) | (_| \__ \  __/
   |____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|*/

  service.get = function (index) {
    return $q(function (resolve, reject) {
      database.ref(path(index)).once('value').then(function (snapshot) {
        if(!snapshot)
          reject();
        else
          resolve(snapshot.val());
      });
    });
  }

  service.set = function (index, data) {
    return $q(function (resolve, reject) {
      database.ref(path(index)).set(data).then(function(){
          resolve();
      }, function(error){
          reject(error);
      });
    });
  }

  service.update = function (index, data) {
    return $q(function (resolve, reject) {
      database.ref(path(index)).update(data).then(function(){
          resolve();
      }, function(error){
          reject(error);
      });
    });
  }

  service.push = function (index, data) {
    return $q(function (resolve, reject) {
      var push = database.ref(path(index)).push(data);
      push.then(function(){
          resolve(push.key);
      }, function(error){
          reject(error);
      });
    });
  }

  service.remove = function () {
    return $q(function(resolve, reject){
      database.ref(path(index)).remove().then(function(){
        resolve();
      }, function (error) {
        reject(error);
      });
    });
  }

  function path (array) {
    if (!array || array.length == 0)
      return "/";

    var ref = "";
    for (var i in array) {
      ref += array[i] + "/";
    }
    return ref;
  }

  return service;
});

