'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.run(function ($state, $localStorage, $rootScope, $firebase) {
  $firebase.get(['_fase']).then(function (fase) {
    if(fase == 2){
      return $state.go('main.ganadores');
    } else {
      if(!$localStorage.usuario)
        return $state.go('main.login');
      else
        $state.go('main.registroNominador');
    } 
  });
})
.config(function ($stateProvider, $ionicConfigProvider) {

  $ionicConfigProvider.backButton.previousTitleText(false).text('');

  $stateProvider
    .state('main', {
    url: '/main',
    abstract: true,
    templateUrl: 'main/templates/menu.html',
    controller: 'MenuCtrl as menu'
  })
    .state('main.list', {
    url: '/list',
    views: {
      'pageContent': {
        templateUrl: 'main/templates/list.html',
        // controller: '<someCtrl> as ctrl'
      }
    }
  })
    .state('main.listDetail', {
    url: '/list/detail',
    views: {
      'pageContent': {
        templateUrl: 'main/templates/list-detail.html',
        // controller: '<someCtrl> as ctrl'
      }
    }
  })

    .state('main.debug', {
    url: '/debug',
    views: {
      'pageContent': {
        templateUrl: 'main/templates/debug.html',
        controller: 'DebugCtrl as ctrl'
      }
    }
  });
});
